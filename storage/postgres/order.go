package postgres

import (
	"context"
	"fmt"
	pb "order_service/genproto/order_protos"
	"order_service/storage"

 
	"github.com/jackc/pgx/v5/pgxpool"
)

type orderRepo struct {
	DB *pgxpool.Pool
}

func NewOrderRepo (db *pgxpool.Pool) storage.IOrderStorage{
	return &orderRepo{
		DB: db,
	}
}

func (o *orderRepo) Create(ctx context.Context, createOrder *pb.CreateOrderRequest) (*pb.Order, error) {
	var (
		order = pb.Order{}
	)

	query := `
		insert into orders (client_id, branch_id, type, address, courier_id, price,
	delivery_price, discount, order_status, payment_type)
		values ($1, $2, $3, $4, $5, $6, $7,$8, $9, $10)
		returning id, client_id, branch_id, type::text, address, courier_id, price,
		delivery_price, discount, order_status::text, payment_type::text
	 `

	err := o.DB.QueryRow(ctx, query,
			createOrder.GetClientId(), 
			createOrder.GetBranchId(),
			createOrder.GetType(),
			createOrder.GetAddress(),
			createOrder.GetCourierId(),
			createOrder.GetPrice(),
			createOrder.GetDeliveryPrice(),
			createOrder.GetDiscount(),
			createOrder.GetOrderStatus(),
			createOrder.GetPaymentType(),
		).Scan(
			&order.Id,
			&order.ClientId,
			&order.BranchId,
			&order.Type,
			&order.Address,
			&order.CourierId,
			&order.Price,
			&order.DeliveryPrice,
			&order.Discount,
			&order.OrderStatus,
			&order.PaymentType,
		)
		if err != nil {
			fmt.Println("error while creating Order!", err.Error())
			return nil, err
	 	} 

		return &order, nil
}

func (o *orderRepo) Get(ctx context.Context, key *pb.OrderPrimaryKey) (*pb.Order, error) {
	var (
		order = pb.Order{}
	)

	query := `select id, client_id, branch_id, type::text, address, courier_id, price,
		delivery_price, discount, order_status::text, payment_type::text,
	created_at::text , updated_at::text from orders
			WHERE deleted_at = 0 and id = $1 `

	err := o.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&order.Id,
		&order.ClientId,
		&order.BranchId,
		&order.Type,
		&order.Address,
		&order.CourierId,
		&order.Price,
		&order.DeliveryPrice,
		&order.Discount,
		&order.OrderStatus,
		&order.PaymentType,
		&order.CreatedAt,
		&order.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting Order by id!", err.Error())
		return nil, err
	}

 	return &order, nil
}

func (o *orderRepo) GetList(ctx context.Context, request *pb.GetOrderListRequest) (*pb.OrdersResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		orders = pb.OrdersResponse{}
		filter, countQuery string
		search = request.Search
	)
// search: id, client_id, branch_id, type, courier_id, price, payment_type

	countQuery = `select count(1) from orders WHERE deleted_at = 0 ` 

	filter = fmt.Sprintf(` AND id::text ilike '%%%s%%' OR type::text ilike '%%%s%%' OR price::text ilike '%%%s%%'
		OR payment_type::text ilike '%%%s%%' `,
			search, search, search, search)
	
	if search != ""{
		countQuery += filter
	}

	err := o.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of Orders",err.Error())
		return nil, err
	}

	query := ` select id, client_id, branch_id, type::text, address, courier_id, price,
	delivery_price, discount, order_status::text, payment_type::text,
created_at::text , updated_at::text from orders
		WHERE deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := o.DB.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting Orders!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			order = pb.Order{}
		)

		err = rows.Scan(
			&order.Id,
			&order.ClientId,
			&order.BranchId,
			&order.Type,
			&order.Address,
			&order.CourierId,
			&order.Price,
			&order.DeliveryPrice,
			&order.Discount,
			&order.OrderStatus,
			&order.PaymentType,
			&order.CreatedAt,
			&order.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning Orders!",err.Error())
			return nil, err
		}
		orders.Orders = append(orders.Orders, &order)
	}

	orders.Count = count

	return &orders, nil
}

func (o *orderRepo) Update(ctx context.Context, updOrder *pb.UpdateOrder) (*pb.Order, error) {
	order := pb.Order{}

	query :=  `UPDATE orders SET client_id = $1, branch_id = $2, type = $3, address = $4,
			courier_id = $5, price = $6, delivery_price = $7, discount = $8, order_status = $9,
		payment_type = $10, updated_at = now() WHERE id = $11 and deleted_at = 0 
			returning id, client_id, branch_id, type::text, address, courier_id, price,
		delivery_price, discount, order_status::text, payment_type::text, updated_at::text `

	err := o.DB.QueryRow(ctx, query,
		updOrder.GetClientId(), 
		updOrder.GetBranchId(),
		updOrder.GetType(),
		updOrder.GetAddress(),
		updOrder.GetCourierId(),
		updOrder.GetPrice(),
		updOrder.GetDeliveryPrice(),
		updOrder.GetDiscount(),
		updOrder.GetOrderStatus(),
		updOrder.GetPaymentType(),
		updOrder.GetId(),
	).Scan(
		&order.Id,
		&order.ClientId,
		&order.BranchId,
		&order.Type,
		&order.Address,
		&order.CourierId,
		&order.Price,
		&order.DeliveryPrice,
		&order.Discount,
		&order.OrderStatus,
		&order.PaymentType,
 		&order.UpdatedAt,
	)

	if err != nil{
		fmt.Println("error while updating Orders!", err.Error())
		return nil, err
	}

	return &order, nil
}

func (o *orderRepo) Delete(ctx context.Context, key *pb.OrderPrimaryKey) (error) {
	query := `UPDATE orders set deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := o.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting Order by id!", err.Error())
		return err
	}
	return nil
}

func (o *orderRepo) UpdateOrderStatus(ctx context.Context, updOrderStatus *pb.UpdateOrderStatusRequest) (*pb.Order,error) {
	order := pb.Order{}

	query := `UPDATE orders SET order_status = $1 
			WHERE id = $2 
		returning id, client_id, price, delivery_price, discount `
	err := o.DB.QueryRow(ctx, query, updOrderStatus.GetOrderStatus(), updOrderStatus.GetId()).Scan(
		&order.Id,
		&order.ClientId,
		&order.Price,
		&order.DeliveryPrice,
		&order.Discount,
	)
	if err != nil{
		fmt.Println("Error while updating order_status!",err.Error())
		return nil,err
	}
	return &order,nil
}