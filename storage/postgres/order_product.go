package postgres

import (
	"context"
	"fmt"
	pb "order_service/genproto/order_protos"
	"order_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type orderProductRepo struct {
	DB *pgxpool.Pool
}

func NewOrderProductRepo (db *pgxpool.Pool) storage.IOrderProductStorage{
	return &orderProductRepo{
		DB: db,
	}
}

func (o *orderProductRepo) Create(ctx context.Context, createOrderProduct *pb.CreateOrderProductRequest) (*pb.OrderProduct, error) {
	var (
		id = uuid.New()
		orderProduct = pb.OrderProduct{}
	)

 

	query := `
		insert into order_products (id, product_id, order_id, quantity, price)
		values ($1, $2, $3, $4, $5)
	returning id, product_id, order_id, quantity, price, created_at::text `

	err := o.DB.QueryRow(ctx, query,
		 	id, 
			createOrderProduct.GetProductId(), 
			createOrderProduct.GetOrderId(),
			createOrderProduct.GetQuantity(),
			createOrderProduct.GetPrice(),
 
		).Scan(
			&orderProduct.Id,
			&orderProduct.ProductId,
			&orderProduct.OrderId,
			&orderProduct.Quantity,
			&orderProduct.Price,
			&orderProduct.CreatedAt,
		)
		if err != nil {
			fmt.Println("error while creating OrderProduct!", err.Error())
			return nil, err
	 	} 

		return &orderProduct, nil
}

func (o *orderProductRepo) Get(ctx context.Context, key *pb.OrderProductPrimaryKey) (*pb.OrderProduct, error) {
	var (
		orderProduct = pb.OrderProduct{}
	)

	query := `select id, product_id, order_id, quantity, price, created_at::text,
		 updated_at::text from order_products
				WHERE deleted_at = 0 and id = $1 `

	err := o.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&orderProduct.Id,
		&orderProduct.ProductId,
		&orderProduct.OrderId,
		&orderProduct.Quantity,
		&orderProduct.Price,
		&orderProduct.CreatedAt,
		&orderProduct.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting OrderProduct by id!", err.Error())
		return nil, err
	}

 	return &orderProduct, nil
}

func (o *orderProductRepo) GetList(ctx context.Context, request *pb.GetOrderProductListRequest) (*pb.OrderProductsResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		orderProducts = pb.OrderProductsResponse{}
		filter, countQuery string
		search = request.Search
	)
// search: product_id

	countQuery = `select count(1) from order_products WHERE deleted_at = 0 ` 

	filter = fmt.Sprintf(` AND product_id ilike '%%%s%%' `,search)
	
	if search != ""{
		countQuery += filter
	}

	err := o.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of OrderProducts",err.Error())
		return nil, err
	}

	query := ` select id, product_id, order_id, quantity, price, created_at::text,
	updated_at::text from order_products
		   WHERE deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := o.DB.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting OrderProducts!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			orderProduct = pb.OrderProduct{}
		)

		err = rows.Scan(
			&orderProduct.Id,
			&orderProduct.ProductId,
			&orderProduct.OrderId,
			&orderProduct.Quantity,
			&orderProduct.Price,
			&orderProduct.CreatedAt,
			&orderProduct.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning OrderProducts!",err.Error())
			return nil, err
		}
		orderProducts.OrderProducts = append(orderProducts.OrderProducts, &orderProduct)
	}

	orderProducts.Count = count

	return &orderProducts, nil
}

func (o *orderProductRepo) Update(ctx context.Context, updOrderProduct *pb.UpdateOrderProduct) (*pb.OrderProduct, error) {
	orderProduct := pb.OrderProduct{}

	query :=  `UPDATE order_products SET product_id = $1, order_id = $2, quantity = $3, price = $4,
			updated_at = now() WHERE deleted_at = 0 AND id = $5
		returning id, product_id, order_id, quantity, price, updated_at::text `

	err := o.DB.QueryRow(ctx, query,
		updOrderProduct.GetProductId(), 
		updOrderProduct.GetOrderId(),
		updOrderProduct.GetQuantity(),
		updOrderProduct.GetPrice(),
		updOrderProduct.GetId(),
	).Scan(
		&orderProduct.Id,
		&orderProduct.ProductId,
		&orderProduct.OrderId,
		&orderProduct.Quantity,
		&orderProduct.Price,
 		&orderProduct.UpdatedAt,
	)

	if err != nil{
		fmt.Println("error while updating OrderProducts!", err.Error())
		return nil, err
	}

	return &orderProduct, nil
}

func (o *orderProductRepo) Delete(ctx context.Context, key *pb.OrderProductPrimaryKey) (error) {
	query := `UPDATE order_products set deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := o.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting OrderProduct by id!", err.Error())
		return err
	}
	return nil
}