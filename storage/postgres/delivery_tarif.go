package postgres

import (
	"context"
	"fmt"
	pb "order_service/genproto/order_protos"
	"order_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type deliveryTarifRepo struct {
	DB *pgxpool.Pool
}

func NewDeliveryTarifRepo (db *pgxpool.Pool) storage.IDeliveryTarifStorage{
	return &deliveryTarifRepo{
		DB: db,
	}
}

func (d *deliveryTarifRepo) Create(ctx context.Context, createDeliveryTarif *pb.CreateDeliveryTarifRequest) (*pb.DeliveryTarif, error) {
	var (
		id = uuid.New()
		deliveryTarif = pb.DeliveryTarif{}
	)

	// string id = 1;
    // string name = 2;
    // string type = 3;
    // float base_price = 4;
    // string delivery_tarif_alternative_id = 5;
    // string created_at = 6;
    // string updated_at = 7;

	query := `
		insert into delivery_tarifs (id, name, type, base_price, delivery_tarif_alternative_id)
			values ($1, $2, $3, $4, $5)
	returning id, name, type::text, base_price, delivery_tarif_alternative_id, created_at::text `

	err := d.DB.QueryRow(ctx, query,
		 	id, 
			createDeliveryTarif.GetName(), 
			createDeliveryTarif.GetType(), 
			createDeliveryTarif.GetBasePrice(), 
			createDeliveryTarif.GetDeliveryTarifAlternativeId(), 
		).Scan(
			&deliveryTarif.Id,
			&deliveryTarif.Name,
 			&deliveryTarif.Type,
 			&deliveryTarif.BasePrice,
 			&deliveryTarif.DeliveryTarifAlternativeId,
 			&deliveryTarif.CreatedAt,
		)
		if err != nil {
			fmt.Println("error while creating DeliveryTarif!", err.Error())
			return nil, err
	 	} 

		return &deliveryTarif, nil
}

func (d *deliveryTarifRepo) Get(ctx context.Context, key *pb.DeliveryTarifPrimaryKey) (*pb.DeliveryTarif, error) {
	var (
		deliveryTarif = pb.DeliveryTarif{}
	)

	query := `select id, name, type::text, base_price, delivery_tarif_alternative_id, created_at::text,
		 updated_at::text from delivery_tarifs
				WHERE deleted_at = 0 and id = $1 `

	err := d.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&deliveryTarif.Id,
		&deliveryTarif.Name,
		&deliveryTarif.Type,
		&deliveryTarif.BasePrice,
		&deliveryTarif.DeliveryTarifAlternativeId,
		&deliveryTarif.CreatedAt,
		&deliveryTarif.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting DeliveryTarif by id!", err.Error())
		return nil, err
	}

 	return &deliveryTarif, nil
}

func (d *deliveryTarifRepo) GetList(ctx context.Context, request *pb.GetDeliveryTarifListRequest) (*pb.DeliveryTarifsResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		deliveryTarifs = pb.DeliveryTarifsResponse{}
		filter, countQuery string
		search = request.Search
	)
// search: name, type

	countQuery = `select count(1) from delivery_tarifs WHERE deleted_at = 0 ` 

	filter = fmt.Sprintf(` AND name ilike '%%%s%%' OR type::text ilike '%%%s%%' `,search, search)
	
	if search != ""{
		countQuery += filter
	}

	err := d.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of DeliveryTarifs",err.Error())
		return nil, err
	}

	query := ` select id, name, type::text, base_price, delivery_tarif_alternative_id, created_at::text,
	updated_at::text from delivery_tarifs
		   WHERE deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := d.DB.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting DeliveryTarifs!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			deliveryTarif = pb.DeliveryTarif{}
		)

		err = rows.Scan(
			&deliveryTarif.Id,
			&deliveryTarif.Name,
			&deliveryTarif.Type,
			&deliveryTarif.BasePrice,
			&deliveryTarif.DeliveryTarifAlternativeId,
			&deliveryTarif.CreatedAt,
			&deliveryTarif.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning DeliveryTarifs!",err.Error())
			return nil, err
		}
		deliveryTarifs.DeliveryTarifs = append(deliveryTarifs.DeliveryTarifs, &deliveryTarif)
	}

	deliveryTarifs.Count = count

	return &deliveryTarifs, nil
}

func (d *deliveryTarifRepo) Update(ctx context.Context, updDeliveryTarif *pb.UpdateDeliveryTarif) (*pb.DeliveryTarif, error) {
	deliveryTarif := pb.DeliveryTarif{}

	query := `UPDATE delivery_tarifs SET name = $1, type = $2, base_price = $3, delivery_tarif_alternative_id = $4, updated_at = now()
					WHERE deleted_at = 0 AND id = $5 
			returning id, name, type, base_price, delivery_tarif_alternative_id, updated_at `

	err := d.DB.QueryRow(ctx, query,
		updDeliveryTarif.GetName(), 
		updDeliveryTarif.GetType(),
		updDeliveryTarif.GetBasePrice(),
		updDeliveryTarif.GetDeliveryTarifAlternativeId(),
		updDeliveryTarif.GetId(),
	).Scan(
		&deliveryTarif.Id,
		&deliveryTarif.Name,
 		&deliveryTarif.Type,
 		&deliveryTarif.BasePrice,
 		&deliveryTarif.DeliveryTarifAlternativeId,
 		&deliveryTarif.UpdatedAt,
	)

	if err != nil{
		fmt.Println("error while updating DeliveryTarifs!", err.Error())
		return nil, err
	}

	return &deliveryTarif, nil
}

func (d *deliveryTarifRepo) Delete(ctx context.Context, key *pb.DeliveryTarifPrimaryKey) (error) {
	query := `UPDATE delivery_tarifs SET deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := d.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting DeliveryTarif by id!", err.Error())
		return err
	}
	return nil
}