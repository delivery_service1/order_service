package postgres

import (
	"order_service/config"
	"order_service/storage"
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
)

type Store struct {
	db *pgxpool.Pool
	cfg config.Config
}

func New (ctx context.Context, cfg config.Config)(storage.IStorage, error){

	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)


	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil{
		fmt.Println("Error while parsing config!",err.Error())
		return Store{}, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil{
		fmt.Println("Error while getting new pool!", err.Error())
		return Store{},err
	}
	
	return Store{
		db: pool,
		cfg: cfg,
	}, nil
}

func (s Store) Close(){
	s.db.Close()
}

func (s Store) Order() storage.IOrderStorage{
	return NewOrderRepo(s.db)
}

func (s Store) OrderProduct() storage.IOrderProductStorage {
	return NewOrderProductRepo(s.db)
}

func (s Store) DeliveryTarifAlternative() storage.IDeliveryTarifAlternativeStorage {
	return NewDeliveryTarifAlternativeRepo(s.db)
}

func (s Store) DeliveryTarif() storage.IDeliveryTarifStorage {
	return NewDeliveryTarifRepo(s.db)
}