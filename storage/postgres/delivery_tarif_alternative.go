package postgres

import (
	"context"
	"fmt"
	pb "order_service/genproto/order_protos"
	"order_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type deliveryTarifAlternativeRepo struct {
	DB *pgxpool.Pool
}

func NewDeliveryTarifAlternativeRepo (db *pgxpool.Pool) storage.IDeliveryTarifAlternativeStorage{
	return &deliveryTarifAlternativeRepo{
		DB: db,
	}
}

func (d *deliveryTarifAlternativeRepo) Create(ctx context.Context, createDeliveryTarifAlternative *pb.CreateDeliveryTarifAlternativeRequest) (*pb.DeliveryTarifAlternative, error) {
	var (
		id = uuid.New()
		deliveryTarifAlternative = pb.DeliveryTarifAlternative{}
	)

	query := `
		insert into delivery_tarif_alternatives (id, type)
		values ($1, $2)
	returning id, type::text, created_at::text `

	err := d.DB.QueryRow(ctx, query,
		 	id, 
			createDeliveryTarifAlternative.GetType(), 
		).Scan(
			&deliveryTarifAlternative.Id,
			&deliveryTarifAlternative.Type,
 			&deliveryTarifAlternative.CreatedAt,
		)
		if err != nil {
			fmt.Println("error while creating DeliveryTarifAlternative!", err.Error())
			return nil, err
	 	} 

		return &deliveryTarifAlternative, nil
}

func (o *deliveryTarifAlternativeRepo) Get(ctx context.Context, key *pb.DeliveryTarifAlternativePrimaryKey) (*pb.DeliveryTarifAlternative, error) {
	var (
		deliveryTarifAlternative = pb.DeliveryTarifAlternative{}
	)

	query := `select id, type::text, created_at::text,
		 updated_at::text from delivery_tarif_alternatives
				WHERE deleted_at = 0 and id = $1 `

	err := o.DB.QueryRow(ctx,query,key.GetId()).Scan(
		&deliveryTarifAlternative.Id,
		&deliveryTarifAlternative.Type,
		&deliveryTarifAlternative.CreatedAt,
		&deliveryTarifAlternative.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting DeliveryTarifAlternative by id!", err.Error())
		return nil, err
	}

 	return &deliveryTarifAlternative, nil
}

func (o *deliveryTarifAlternativeRepo) GetList(ctx context.Context, request *pb.GetDeliveryTarifAlternativeListRequest) (*pb.DeliveryTarifAlternativesResponse, error) {
	var (
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
		deliveryTarifAlternatives = pb.DeliveryTarifAlternativesResponse{}
		filter, countQuery string
		search = request.Search
	)
// search: 

	countQuery = `select count(1) from delivery_tarif_alternatives WHERE deleted_at = 0 ` 

	filter = fmt.Sprintf(` AND type ilike '%%%s%%' `,search)
	
	if search != ""{
		countQuery += filter
	}

	err := o.DB.QueryRow(ctx, countQuery).Scan(&count)
	if err != nil {
		fmt.Println("error while selecting count of DeliveryTarifAlternatives",err.Error())
		return nil, err
	}
	
	query := ` select id, type::text, created_at::text,
	updated_at::text from delivery_tarif_alternatives
		   WHERE deleted_at = 0 `

	if search != ""{
		query += filter
	}	

	query += ` OFFSET $1 LIMIT $2`

	rows, err := o.DB.Query(ctx, query, offset, request.GetLimit())
	if err != nil {
		fmt.Println("error while selecting DeliveryTarifAlternatives!",err.Error())
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			deliveryTarifAlternative = pb.DeliveryTarifAlternative{}
		)

		err = rows.Scan(
			&deliveryTarifAlternative.Id,
			&deliveryTarifAlternative.Type,
			&deliveryTarifAlternative.CreatedAt,
			&deliveryTarifAlternative.UpdatedAt,
		)
		if err != nil {
			fmt.Println("error while scanning DeliveryTarifAlternatives!",err.Error())
			return nil, err
		}
		deliveryTarifAlternatives.DeliveryTarifAlternatives = append(deliveryTarifAlternatives.DeliveryTarifAlternatives, &deliveryTarifAlternative)
	}

	deliveryTarifAlternatives.Count = count

	return &deliveryTarifAlternatives, nil
}

func (o *deliveryTarifAlternativeRepo) Update(ctx context.Context, updDeliveryTarifAlternative *pb.UpdateDeliveryTarifAlternative) (*pb.DeliveryTarifAlternative, error) {
	deliveryTarifAlternative := pb.DeliveryTarifAlternative{}

	query :=  `UPDATE delivery_tarif_alternatives SET type = $1,
			updated_at = now() WHERE deleted_at = 0 AND id = $2
		returning id, type::text, updated_at::text `

	err := o.DB.QueryRow(ctx, query,
		updDeliveryTarifAlternative.GetType(), 
		updDeliveryTarifAlternative.GetId(),
	).Scan(
		&deliveryTarifAlternative.Id,
		&deliveryTarifAlternative.Type,
 		&deliveryTarifAlternative.UpdatedAt,
	)

	if err != nil{
		fmt.Println("error while updating DeliveryTarifAlternatives!", err.Error())
		return nil, err
	}

	return &deliveryTarifAlternative, nil
}

func (o *deliveryTarifAlternativeRepo) Delete(ctx context.Context, key *pb.DeliveryTarifAlternativePrimaryKey) (error) {
	query := `UPDATE delivery_tarif_alternatives set deleted_at = 1 where id = $1 AND deleted_at = 0`

	_, err := o.DB.Exec(ctx,query,key.GetId())
	if err != nil{
		fmt.Println("error while deleting DeliveryTarifAlternative by id!", err.Error())
		return err
	}
	return nil
}