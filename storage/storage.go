package storage

import (
	"context"
	pb "order_service/genproto/order_protos"
)

type IStorage interface {
	Close()
	Order() IOrderStorage
	OrderProduct() IOrderProductStorage
	DeliveryTarifAlternative() IDeliveryTarifAlternativeStorage
	DeliveryTarif() IDeliveryTarifStorage
}

type IOrderStorage interface {
	Create(context.Context, *pb.CreateOrderRequest) (*pb.Order, error)
	Get(context.Context, *pb.OrderPrimaryKey) (*pb.Order, error)
	GetList(context.Context, *pb.GetOrderListRequest) (*pb.OrdersResponse, error)
	Update(context.Context, *pb.UpdateOrder) (*pb.Order, error)
	Delete(context.Context, *pb.OrderPrimaryKey) (error)
	UpdateOrderStatus(context.Context, *pb.UpdateOrderStatusRequest) (*pb.Order,error)
}

type IOrderProductStorage interface {
	Create(context.Context, *pb.CreateOrderProductRequest) (*pb.OrderProduct, error)
	Get(context.Context, *pb.OrderProductPrimaryKey) (*pb.OrderProduct, error)
	GetList(context.Context, *pb.GetOrderProductListRequest) (*pb.OrderProductsResponse, error)
	Update(context.Context, *pb.UpdateOrderProduct) (*pb.OrderProduct, error)
	Delete(context.Context, *pb.OrderProductPrimaryKey) (error)
}

type IDeliveryTarifAlternativeStorage interface {
	Create(context.Context, *pb.CreateDeliveryTarifAlternativeRequest) (*pb.DeliveryTarifAlternative, error)
	Get(context.Context, *pb.DeliveryTarifAlternativePrimaryKey) (*pb.DeliveryTarifAlternative, error)
	GetList(context.Context, *pb.GetDeliveryTarifAlternativeListRequest) (*pb.DeliveryTarifAlternativesResponse, error)
	Update(context.Context, *pb.UpdateDeliveryTarifAlternative) (*pb.DeliveryTarifAlternative, error)
	Delete(context.Context, *pb.DeliveryTarifAlternativePrimaryKey) (error)
}
 
type IDeliveryTarifStorage interface {
	Create(context.Context, *pb.CreateDeliveryTarifRequest) (*pb.DeliveryTarif, error)
	Get(context.Context, *pb.DeliveryTarifPrimaryKey) (*pb.DeliveryTarif, error)
	GetList(context.Context, *pb.GetDeliveryTarifListRequest) (*pb.DeliveryTarifsResponse, error)
	Update(context.Context, *pb.UpdateDeliveryTarif) (*pb.DeliveryTarif, error)
	Delete(context.Context, *pb.DeliveryTarifPrimaryKey) (error)
}
