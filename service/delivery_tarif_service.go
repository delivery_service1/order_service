package service

import (
	pb "order_service/genproto/order_protos"
	"order_service/grpc/client"
	"order_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type deliveryTarifService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedDeliveryTarifServiceServer
}

func NewDeliveryTarifService(strg storage.IStorage, services client.IServiceManager) *deliveryTarifService {
	return &deliveryTarifService{
		storage: strg,
		serivces: services,
	}
}

func (d *deliveryTarifService) Create(ctx context.Context, request *pb.CreateDeliveryTarifRequest) (*pb.DeliveryTarif, error) {
	deliveryTarif, err := d.storage.DeliveryTarif().Create(ctx, request)
	if err != nil {
		fmt.Println("Error in service, while creating deliveryTarif!", err.Error())
		return nil, err
	}
	return deliveryTarif, nil
}

func (d *deliveryTarifService) Get(ctx context.Context, request *pb.DeliveryTarifPrimaryKey) (*pb.DeliveryTarif, error) {
	deliveryTarif, err := d.storage.DeliveryTarif().Get(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting deliveryTarif!",err.Error())
		return nil, err
	}
	return deliveryTarif, nil
}

func (d *deliveryTarifService) GetList(ctx context.Context, request *pb.GetDeliveryTarifListRequest) (*pb.DeliveryTarifsResponse, error) {
	deliveryTarifs, err := d.storage.DeliveryTarif().GetList(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting deliveryTarifs list!", err.Error())
		return nil, err
	}
	return deliveryTarifs, nil
}

func (d *deliveryTarifService) Update(ctx context.Context, request *pb.UpdateDeliveryTarif) (*pb.DeliveryTarif, error) {
	deliveryTarif, err := d.storage.DeliveryTarif().Update(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating deliveryTarif!", err.Error())
		return nil, err
	}
	return deliveryTarif, nil
}

func (d *deliveryTarifService) Delete(ctx context.Context, key *pb.DeliveryTarifPrimaryKey) (*emptypb.Empty, error) {
	err := d.storage.DeliveryTarif().Delete(ctx,key)
	if err != nil{
		fmt.Println("Error in service, while deleting deliveryTarif!", err.Error())
		return nil,err
	}
	return nil,nil
}


