package service

import (
	"context"
	"fmt"
	"order_service/email"
	pb "order_service/genproto/order_protos"
	pbu "order_service/genproto/user_protos"
	"order_service/grpc/client"
	"order_service/storage"
	"google.golang.org/protobuf/types/known/emptypb"
)

type orderService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedOrderServiceServer
}

func NewOrderService(strg storage.IStorage, services client.IServiceManager) *orderService {
	return &orderService{
		storage: strg,
		serivces: services,
	}
}

func (o *orderService) Create(ctx context.Context, request *pb.CreateOrderRequest) (*pb.Order, error) {
	_, err := o.serivces.BranchService().Get(ctx,&pbu.BranchPrimaryKey{
		Id: request.BranchId,
	})
	if err != nil{
		fmt.Println("Error, cannot find a branch!", err.Error())
		return nil, err
	}

	_, err = o.serivces.CourierService().Get(ctx,&pbu.CourierPrimaryKey{
		Id: request.CourierId,
	})

	if err != nil{
		fmt.Println("Error, cannot find a courier!", err.Error())
		return nil, err
	}

	client, err := o.serivces.ClientService().GetDiscountType(ctx,&pbu.ClientPrimaryKey{
		Id: request.ClientId,
	})
	if err != nil{
		fmt.Println("Error in service, while getting client discount type!",err.Error())
		return nil, err
	}
	
	if client.DiscountType == "percent"{
		request.Discount = (request.Price+request.DeliveryPrice)/0.05
	}

	order, err := o.storage.Order().Create(ctx, request)
	if err != nil {
		fmt.Println("Error in service, while creating Order!", err.Error())
		return nil, err
	}

	totalOrdersSum := (request.Price-request.Discount)+request.DeliveryPrice

	if order.GetOrderStatus() == "accepted"{
		    _, err := o.serivces.ClientService().UpdateInfo(ctx,&pbu.UpdateClientInfo{
			Id: request.ClientId,
 			TotalOrdersSum: totalOrdersSum,
			TotalOrdersCount: 1,
			DiscountAmount: request.Discount,
		})
		if err != nil{
			fmt.Println("Error in order service, while updating client info!", err.Error())
			return nil, err
		}
	}
 
	body :=	`<center><h1>Assalamu alaikum</h1></center>

<h4>Thank you for choosing The Delivery Service.</h4>
<h4>Your order id is: %s </h4>
<h4>Your address: %s </h4>
<h4>Payment type: %s </h4>
<h4>The Order Price is: %v </h4>
<h4>Your Delivery Price is: %v </h4>
<h4>Your Discount is: %v </h4>
<h4>The Total Sum is: %v </h4>
<h4>If have you any questions you can call: +998123456789!</h4>`

body = fmt.Sprintf(body,order.Id,request.GetAddress(),request.GetPaymentType(),request.GetPrice(),request.DeliveryPrice,
				request.Discount, totalOrdersSum)

	if err := email.SendMessage(body, "khozhakbar55@gmail.com"); err != nil {
		fmt.Printf("Error while sending email to %s error is: %v\n", "static", err)
	}
	return order, nil	
}

func (o *orderService) Get(ctx context.Context, request *pb.OrderPrimaryKey) (*pb.Order, error) {
	order, err := o.storage.Order().Get(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting Order!",err.Error())
		return nil, err
	}
	return order, nil
}

func (o *orderService) GetList(ctx context.Context, request *pb.GetOrderListRequest) (*pb.OrdersResponse, error) {
	orders, err := o.storage.Order().GetList(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting Orders list!", err.Error())
		return nil, err
	}
	return orders, nil
}

func (o *orderService) Update(ctx context.Context, request *pb.UpdateOrder) (*pb.Order, error) {
	_, err := o.serivces.BranchService().Get(ctx,&pbu.BranchPrimaryKey{
		Id: request.BranchId,
	})
	if err != nil{
		fmt.Println("Error, cannot find a branch!", err.Error())
		return nil, err
	}

	_, err = o.serivces.CourierService().Get(ctx,&pbu.CourierPrimaryKey{
		Id: request.CourierId,
	})

	if err != nil{
		fmt.Println("Error, cannot find a courier!", err.Error())
		return nil, err
	}

	client, err := o.serivces.ClientService().GetDiscountType(ctx,&pbu.ClientPrimaryKey{
		Id: request.ClientId,
	})
	if err != nil{
		fmt.Println("Error in service, while getting client discount type!",err.Error())
		return nil, err
	}
	
	if client.DiscountType == "percent"{
		request.Discount = (request.Price+request.DeliveryPrice)/0.05
	}

	order, err := o.storage.Order().Update(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating Order!", err.Error())
		return nil, err
	}

	totalOrdersSum := (order.Price-order.Discount)+order.DeliveryPrice

	if order.GetOrderStatus() == "accepted"{
		_, err := o.serivces.ClientService().UpdateInfo(ctx,&pbu.UpdateClientInfo{
		Id: order.GetClientId(),
		TotalOrdersSum: totalOrdersSum,
		TotalOrdersCount: 1,
	})
		if err != nil{
			fmt.Println("Error in order service, while updating client info!", err.Error())
			return nil, err
		}
}
	return order, nil
}

func (o *orderService) Delete(ctx context.Context, key *pb.OrderPrimaryKey) (*emptypb.Empty, error) {
	err := o.storage.Order().Delete(ctx,key)
	if err != nil{
		fmt.Println("Error in service, while deleting Order!", err.Error())
		return nil,err
	}
	return nil,nil
}

func (o *orderService) UpdateOrderStatus(ctx context.Context, request *pb.UpdateOrderStatusRequest) (*emptypb.Empty, error) {
	order, err := o.storage.Order().UpdateOrderStatus(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating order status!",err.Error())
		return nil, err
	}

	totalOrdersSum := (order.Price-order.Discount)+order.DeliveryPrice


	if request.GetOrderStatus() == "accepted"{
		_, err := o.serivces.ClientService().UpdateInfo(ctx, &pbu.UpdateClientInfo{
			Id: order.GetClientId(),
			TotalOrdersSum: totalOrdersSum,
			TotalOrdersCount: 1,
		})
		if err != nil{
			fmt.Println("Error in order service, while updating client info!", err.Error())
			return nil, err
		}

	}

	return nil, nil
}
