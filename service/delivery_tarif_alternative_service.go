package service

import (
	pb "order_service/genproto/order_protos"
	"order_service/grpc/client"
	"order_service/storage"
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"
)

type deliveryTarifAlternativeService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedDeliveryTarifAlternativeServiceServer
}

func NewDeliveryTarifAlternativeService(strg storage.IStorage, services client.IServiceManager) *deliveryTarifAlternativeService {
	return &deliveryTarifAlternativeService{
		storage: strg,
		serivces: services,
	}
}

func (d *deliveryTarifAlternativeService) Create(ctx context.Context, request *pb.CreateDeliveryTarifAlternativeRequest) (*pb.DeliveryTarifAlternative, error) {
	deliveryTarifAlternative, err := d.storage.DeliveryTarifAlternative().Create(ctx, request)
	if err != nil {
		fmt.Println("Error in service, while creating deliveryTarifAlternative!", err.Error())
		return nil, err
	}
	return deliveryTarifAlternative, nil
}

func (d *deliveryTarifAlternativeService) Get(ctx context.Context, request *pb.DeliveryTarifAlternativePrimaryKey) (*pb.DeliveryTarifAlternative, error) {
	deliveryTarifAlternative, err := d.storage.DeliveryTarifAlternative().Get(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting deliveryTarifAlternative!",err.Error())
		return nil, err
	}
	return deliveryTarifAlternative, nil
}

func (d *deliveryTarifAlternativeService) GetList(ctx context.Context, request *pb.GetDeliveryTarifAlternativeListRequest) (*pb.DeliveryTarifAlternativesResponse, error) {
	deliveryTarifAlternatives, err := d.storage.DeliveryTarifAlternative().GetList(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting deliveryTarifAlternatives list!", err.Error())
		return nil, err
	}
	return deliveryTarifAlternatives, nil
}

func (d *deliveryTarifAlternativeService) Update(ctx context.Context, request *pb.UpdateDeliveryTarifAlternative) (*pb.DeliveryTarifAlternative, error) {
	deliveryTarifAlternative, err := d.storage.DeliveryTarifAlternative().Update(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating deliveryTarifAlternative!", err.Error())
		return nil, err
	}
	return deliveryTarifAlternative, nil
}

func (d *deliveryTarifAlternativeService) Delete(ctx context.Context, key *pb.DeliveryTarifAlternativePrimaryKey) (*emptypb.Empty, error) {
	err := d.storage.DeliveryTarifAlternative().Delete(ctx,key)
	if err != nil{
		fmt.Println("Error in service, while deleting deliveryTarifAlternative!", err.Error())
		return nil,err
	}
	return nil,nil
}


