package service

import (
	"context"
	"fmt"
	pbc "order_service/genproto/catalog_protos"
	pb "order_service/genproto/order_protos"
	"order_service/grpc/client"
	"order_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type orderProductService struct {
	storage storage.IStorage
	serivces client.IServiceManager
	pb.UnimplementedOrderProductServiceServer
}

func NewOrderProductService(strg storage.IStorage, services client.IServiceManager) *orderProductService {
	return &orderProductService{
		storage: strg,
		serivces: services,
	}
}

func (o *orderProductService) Create(ctx context.Context, request *pb.CreateOrderProductRequest) (*pb.OrderProduct, error) {
	_, err := o.serivces.ProductService().Get(ctx,&pbc.ProductPrimaryKey{Id: request.ProductId})
	if err != nil{
		fmt.Println("Error in service, while getting product while creating order product!",err.Error())
		return nil,err
	}

	orderProduct, err := o.storage.OrderProduct().Create(ctx, request)
	if err != nil {
		fmt.Println("Error in service, while creating Order!", err.Error())
		return nil, err
	}
	return orderProduct, nil
}

func (o *orderProductService) Get(ctx context.Context, request *pb.OrderProductPrimaryKey) (*pb.OrderProduct, error) {
	orderProduct, err := o.storage.OrderProduct().Get(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting Order!",err.Error())
		return nil, err
	}
	return orderProduct, nil
}

func (o *orderProductService) GetList(ctx context.Context, request *pb.GetOrderProductListRequest) (*pb.OrderProductsResponse, error) {
	orderProducts, err := o.storage.OrderProduct().GetList(ctx, request)
	if err != nil{
		fmt.Println("Error in service, while getting Orders list!", err.Error())
		return nil, err
	}
	return orderProducts, nil
}

func (o *orderProductService) Update(ctx context.Context, request *pb.UpdateOrderProduct) (*pb.OrderProduct, error) {
	_, err := o.serivces.ProductService().Get(ctx,&pbc.ProductPrimaryKey{Id: request.ProductId})
	if err != nil{
		fmt.Println("Error in service, while getting product while creating order product!",err.Error())
		return nil,err
	}

	orderProduct, err := o.storage.OrderProduct().Update(ctx,request)
	if err != nil{
		fmt.Println("Error in service, while updating Order!", err.Error())
		return nil, err
	}
	return orderProduct, nil
}

func (o *orderProductService) Delete(ctx context.Context, key *pb.OrderProductPrimaryKey) (*emptypb.Empty, error) {
	err := o.storage.OrderProduct().Delete(ctx,key)
	if err != nil{
		fmt.Println("Error in service, while deleting Order!", err.Error())
		return nil,err
	}
	return nil,nil
}


