DROP TYPE IF EXISTS client_discount_type_enum;
DROP TYPE IF EXISTS status_enum;
DROP TYPE IF EXISTS order_type_enum;
DROP TYPE IF EXISTS order_status_enum;
DROP TYPE IF EXISTS payment_type_enum;
DROP TYPE IF EXISTS product_type_enum;
DROP TYPE IF EXISTS delivery_tarif_type_enum;
 
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS products;
DROP TABLE IF EXISTS delivery_tarif_alternatives;
DROP TABLE IF EXISTS delivery_tarifs;
DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS branches;
DROP TABLE IF EXISTS couriers;
DROP TABLE IF EXISTS admins;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS order_products;