DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'status_enum') THEN
        CREATE TYPE status_enum AS ENUM ('yes', 'no');
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'order_type_enum') THEN
        CREATE TYPE order_type_enum AS ENUM ('delivery', 'pick_up');
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'order_status_enum') THEN
        CREATE TYPE order_status_enum AS ENUM (
            'accepted',
            'courier_accepted',
            'ready_in_branch',
            'on_way',
            'finished',
            'canceled'
        );
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'payment_type_enum') THEN
        CREATE TYPE payment_type_enum AS ENUM ('cash', 'card');
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'delivery_tarif_type_enum') THEN
        CREATE TYPE delivery_tarif_type_enum AS ENUM ('fixed', 'alternative');
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'delivery_tarif_alternative_enum') THEN
        CREATE TYPE delivery_tarif_alternative_enum AS ENUM ('from_price', 'to_price', 'price');
    END IF;
END $$;

CREATE TABLE IF NOT EXISTS delivery_tarif_alternatives (
  id UUID PRIMARY KEY,
  type delivery_tarif_alternative_enum,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  deleted_at INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS delivery_tarifs (
  id UUID PRIMARY KEY,
  name VARCHAR(30),
  type delivery_tarif_type_enum,
  base_price NUMERIC,
  delivery_tarif_alternative_id UUID REFERENCES delivery_tarif_alternatives(id),
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  deleted_at INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS orders (
  id VARCHAR(20) PRIMARY KEY,
  client_id UUID,
  branch_id UUID,
  type order_type_enum,
  address VARCHAR(60),
  courier_id UUID,
  price NUMERIC,
  delivery_price NUMERIC,
  discount NUMERIC,
  order_status order_status_enum,
  payment_type payment_type_enum,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  deleted_at INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS order_products (
  id UUID PRIMARY KEY,
  product_id UUID,
  order_id VARCHAR(20) REFERENCES orders(id),
  quantity INT,
  price NUMERIC,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  deleted_at INT DEFAULT 0
);

CREATE SEQUENCE id_generator START WITH 1 INCREMENT BY 1 NO CYCLE;

CREATE OR REPLACE FUNCTION set_order_id()
RETURNS TRIGGER AS $$
BEGIN
  NEW.id = 'A-' || LPAD(NEXTVAL('id_generator')::TEXT, 6, '0');
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER set_order_id_trigger
BEFORE INSERT ON orders
FOR EACH ROW
EXECUTE FUNCTION set_order_id();
