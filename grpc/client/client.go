package client

import (
	"fmt"
	"order_service/config"
	catalog_service "order_service/genproto/catalog_protos"
	"order_service/genproto/order_protos"
	user_service "order_service/genproto/user_protos"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	OrderService()  order_service.OrderServiceClient
	OrderProductService() order_service.OrderProductServiceClient
	DeliveryTarifAlternativeService() order_service.DeliveryTarifAlternativeServiceClient
	DeliveryTarifService() order_service.DeliveryTarifServiceClient

	// User service
	BranchService() user_service.BranchServiceClient
	ClientService() user_service.ClientServiceClient
	CourierService() user_service.CourierServiceClient

	// Catalog service

	ProductService() catalog_service.ProductServiceClient
}

type grpcClients struct {
	orderService order_service.OrderServiceClient
	orderProductService order_service.OrderProductServiceClient
	deliveryTarifAlternativeService order_service.DeliveryTarifAlternativeServiceClient
	deliveryTarifService order_service.DeliveryTarifServiceClient

	branchService user_service.BranchServiceClient
	clientService user_service.ClientServiceClient
	courierService user_service.CourierServiceClient

	productService catalog_service.ProductServiceClient
}

func NewGrpcClients (cfg config.Config)(IServiceManager, error){
	connOrderService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting to api_gateway!",err.Error())
		return nil, err
	}

	connUserService, err := grpc.Dial(
		cfg.UserServiceGrpcHost+cfg.UserServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting to api_gateway!",err.Error())
		return nil, err
	}

	connCatalogService, err := grpc.Dial(
		cfg.CatalogServiceGrpcHost+cfg.CatalogServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while connecting to api_gateway!",err.Error())
		return nil, err
	}

	return &grpcClients{
		orderService: order_service.NewOrderServiceClient(connOrderService),
		orderProductService: order_service.NewOrderProductServiceClient(connOrderService),
		deliveryTarifAlternativeService: order_service.NewDeliveryTarifAlternativeServiceClient(connOrderService),
		deliveryTarifService: order_service.NewDeliveryTarifServiceClient(connOrderService),

		branchService: user_service.NewBranchServiceClient(connUserService),
		clientService: user_service.NewClientServiceClient(connUserService),
		courierService: user_service.NewCourierServiceClient(connUserService),

		productService: catalog_service.NewProductServiceClient(connCatalogService),
	}, nil
}

func (g *grpcClients) OrderService() order_service.OrderServiceClient{
	return g.orderService
}

func (g *grpcClients) OrderProductService() order_service.OrderProductServiceClient {
	return g.orderProductService
}

func (g *grpcClients) DeliveryTarifAlternativeService() order_service.DeliveryTarifAlternativeServiceClient {
	return g.deliveryTarifAlternativeService
}

func (g *grpcClients) DeliveryTarifService() order_service.DeliveryTarifServiceClient {
	return g.deliveryTarifService
}


func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) CourierService() user_service.CourierServiceClient {
	return g.courierService
}
 

func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}