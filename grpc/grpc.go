package grpc

import (
	pb "order_service/genproto/order_protos"
	"order_service/grpc/client"
	"order_service/service"
	"order_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer (strg storage.IStorage, services client.IServiceManager)*grpc.Server{
	grpcServer := grpc.NewServer()
	
	pb.RegisterOrderServiceServer(grpcServer, service.NewOrderService(strg, services))
	pb.RegisterOrderProductServiceServer(grpcServer, service.NewOrderProductService(strg, services))
	pb.RegisterDeliveryTarifAlternativeServiceServer(grpcServer, service.NewDeliveryTarifAlternativeService(strg, services))
	pb.RegisterDeliveryTarifServiceServer(grpcServer, service.NewDeliveryTarifService(strg, services))

	reflection.Register(grpcServer)

	return grpcServer
}